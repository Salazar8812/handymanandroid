// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NextFragment_ViewBinding implements Unbinder {
  private NextFragment target;

  @UiThread
  public NextFragment_ViewBinding(NextFragment target, View source) {
    this.target = target;

    target.mListNextRecyclerView = Utils.findRequiredViewAsType(source, R.id.mListNextRecyclerView, "field 'mListNextRecyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    NextFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mListNextRecyclerView = null;
  }
}
