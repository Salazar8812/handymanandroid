// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu.Fragments;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TodayFragment_ViewBinding implements Unbinder {
  private TodayFragment target;

  private View view2131296460;

  @UiThread
  public TodayFragment_ViewBinding(final TodayFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.mMoreDetailContentRelative, "method 'OnClickDetail'");
    view2131296460 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickDetail();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view2131296460.setOnClickListener(null);
    view2131296460 = null;
  }
}
