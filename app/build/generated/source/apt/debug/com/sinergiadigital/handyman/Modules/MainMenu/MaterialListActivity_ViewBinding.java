// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MaterialListActivity_ViewBinding implements Unbinder {
  private MaterialListActivity target;

  @UiThread
  public MaterialListActivity_ViewBinding(MaterialListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MaterialListActivity_ViewBinding(MaterialListActivity target, View source) {
    this.target = target;

    target.mSwitch = Utils.findRequiredViewAsType(source, R.id.mSwitch, "field 'mSwitch'", LabeledSwitch.class);
    target.mListMaterialRecyclerView = Utils.findRequiredViewAsType(source, R.id.mListMaterialRecyclerView, "field 'mListMaterialRecyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MaterialListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSwitch = null;
    target.mListMaterialRecyclerView = null;
  }
}
