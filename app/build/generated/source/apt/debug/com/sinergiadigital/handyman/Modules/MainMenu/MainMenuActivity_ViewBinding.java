// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.ViewPagerUtils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainMenuActivity_ViewBinding implements Unbinder {
  private MainMenuActivity target;

  @UiThread
  public MainMenuActivity_ViewBinding(MainMenuActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainMenuActivity_ViewBinding(MainMenuActivity target, View source) {
    this.target = target;

    target.mPager = Utils.findRequiredViewAsType(source, R.id.mPager, "field 'mPager'", ViewPagerUtils.class);
    target.mTabs = Utils.findRequiredViewAsType(source, R.id.mOptionTabs, "field 'mTabs'", TabLayout.class);
    target.mSwitch = Utils.findRequiredViewAsType(source, R.id.mSwitch, "field 'mSwitch'", LabeledSwitch.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainMenuActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mPager = null;
    target.mTabs = null;
    target.mSwitch = null;
  }
}
