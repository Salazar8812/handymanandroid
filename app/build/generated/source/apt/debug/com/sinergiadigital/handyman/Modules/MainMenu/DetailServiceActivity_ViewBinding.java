// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailServiceActivity_ViewBinding implements Unbinder {
  private DetailServiceActivity target;

  private View view2131296437;

  @UiThread
  public DetailServiceActivity_ViewBinding(DetailServiceActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DetailServiceActivity_ViewBinding(final DetailServiceActivity target, View source) {
    this.target = target;

    View view;
    target.mSwitch = Utils.findRequiredViewAsType(source, R.id.mSwitch, "field 'mSwitch'", LabeledSwitch.class);
    view = Utils.findRequiredView(source, R.id.mCancelServiceButton, "field 'mCancelServiceButton' and method 'OnClickCancel'");
    target.mCancelServiceButton = Utils.castView(view, R.id.mCancelServiceButton, "field 'mCancelServiceButton'", Button.class);
    view2131296437 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickCancel();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailServiceActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSwitch = null;
    target.mCancelServiceButton = null;

    view2131296437.setOnClickListener(null);
    view2131296437 = null;
  }
}
