// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu.Dialogs;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DialogErrorRestorePass_ViewBinding implements Unbinder {
  private DialogErrorRestorePass target;

  private View view2131296432;

  @UiThread
  public DialogErrorRestorePass_ViewBinding(DialogErrorRestorePass target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DialogErrorRestorePass_ViewBinding(final DialogErrorRestorePass target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.mAcceptButtom, "field 'mCancelButton' and method 'OnClickCancelButton'");
    target.mCancelButton = Utils.castView(view, R.id.mAcceptButtom, "field 'mCancelButton'", TextView.class);
    view2131296432 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickCancelButton();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DialogErrorRestorePass target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mCancelButton = null;

    view2131296432.setOnClickListener(null);
    view2131296432 = null;
  }
}
