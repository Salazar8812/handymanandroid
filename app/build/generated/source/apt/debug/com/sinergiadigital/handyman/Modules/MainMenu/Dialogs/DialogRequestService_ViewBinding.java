// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu.Dialogs;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DialogRequestService_ViewBinding implements Unbinder {
  private DialogRequestService target;

  private View view2131296433;

  private View view2131296468;

  @UiThread
  public DialogRequestService_ViewBinding(DialogRequestService target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DialogRequestService_ViewBinding(final DialogRequestService target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.mAcceptServiceButton, "method 'OnClickButton'");
    view2131296433 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickButton(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mRefusedServiceButton, "method 'OnClickButton'");
    view2131296468 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickButton(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view2131296433.setOnClickListener(null);
    view2131296433 = null;
    view2131296468.setOnClickListener(null);
    view2131296468 = null;
  }
}
