// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.RestorePassword;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RestorePasswordActivity_ViewBinding implements Unbinder {
  private RestorePasswordActivity target;

  private View view2131296470;

  @UiThread
  public RestorePasswordActivity_ViewBinding(RestorePasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RestorePasswordActivity_ViewBinding(final RestorePasswordActivity target, View source) {
    this.target = target;

    View view;
    target.mNewPasword = Utils.findRequiredViewAsType(source, R.id.mNewPassword, "field 'mNewPasword'", EditText.class);
    target.mConfirmNewPassword = Utils.findRequiredViewAsType(source, R.id.mConfirmNewPassword, "field 'mConfirmNewPassword'", EditText.class);
    view = Utils.findRequiredView(source, R.id.mSaveNewPassword, "method 'onClickSavePassword'");
    view2131296470 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClickSavePassword();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RestorePasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mNewPasword = null;
    target.mConfirmNewPassword = null;

    view2131296470.setOnClickListener(null);
    view2131296470 = null;
  }
}
