// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DetailServiceDoneActivity_ViewBinding implements Unbinder {
  private DetailServiceDoneActivity target;

  private View view2131296454;

  @UiThread
  public DetailServiceDoneActivity_ViewBinding(DetailServiceDoneActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DetailServiceDoneActivity_ViewBinding(final DetailServiceDoneActivity target,
      View source) {
    this.target = target;

    View view;
    target.mSwitch = Utils.findRequiredViewAsType(source, R.id.mSwitch, "field 'mSwitch'", LabeledSwitch.class);
    view = Utils.findRequiredView(source, R.id.mListButton, "method 'OnClickList'");
    view2131296454 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickList(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DetailServiceDoneActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mSwitch = null;

    view2131296454.setOnClickListener(null);
    view2131296454 = null;
  }
}
