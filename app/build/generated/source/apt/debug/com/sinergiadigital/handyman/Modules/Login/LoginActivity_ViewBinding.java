// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.Login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActivity_ViewBinding implements Unbinder {
  private LoginActivity target;

  private View view2131296448;

  private View view2131296469;

  private View view2131296451;

  @UiThread
  public LoginActivity_ViewBinding(LoginActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActivity_ViewBinding(final LoginActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.mEnterButton, "method 'OnClickEnter'");
    view2131296448 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickEnter(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mRegisterButton, "method 'OnClickEnter'");
    view2131296469 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickEnter(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.mForgotPasswordButton, "method 'OnClickEnter'");
    view2131296451 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickEnter(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view2131296448.setOnClickListener(null);
    view2131296448 = null;
    view2131296469.setOnClickListener(null);
    view2131296469 = null;
    view2131296451.setOnClickListener(null);
    view2131296451 = null;
  }
}
