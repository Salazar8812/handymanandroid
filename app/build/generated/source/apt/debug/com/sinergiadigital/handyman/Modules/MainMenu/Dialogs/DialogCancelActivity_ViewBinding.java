// Generated code from Butter Knife. Do not modify!
package com.sinergiadigital.handyman.Modules.MainMenu.Dialogs;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.sinergiadigital.handyman.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DialogCancelActivity_ViewBinding implements Unbinder {
  private DialogCancelActivity target;

  private View view2131296436;

  private View view2131296431;

  @UiThread
  public DialogCancelActivity_ViewBinding(DialogCancelActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public DialogCancelActivity_ViewBinding(final DialogCancelActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.mCancelButton, "field 'mCancelButton' and method 'OnClickCancelButton'");
    target.mCancelButton = Utils.castView(view, R.id.mCancelButton, "field 'mCancelButton'", Button.class);
    view2131296436 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickCancelButton();
      }
    });
    view = Utils.findRequiredView(source, R.id.mAccepConfirmButton, "method 'OnClickConfirmbutton'");
    view2131296431 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnClickConfirmbutton();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    DialogCancelActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mCancelButton = null;

    view2131296436.setOnClickListener(null);
    view2131296436 = null;
    view2131296431.setOnClickListener(null);
    view2131296431 = null;
  }
}
