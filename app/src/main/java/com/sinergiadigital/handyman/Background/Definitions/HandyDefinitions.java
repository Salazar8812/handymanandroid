package com.sinergiadigital.handyman.Background.Definitions;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HandyDefinitions {
    @GET("/servicios/autenticaConductor.php")
    Call<ResponseBody> login(@Query("email")String email, @Query("password")String password);


}
