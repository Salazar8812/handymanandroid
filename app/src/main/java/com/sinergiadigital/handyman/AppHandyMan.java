package com.sinergiadigital.handyman;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.sinergiadigital.handyman.Bases.ApplicationPresenter;
import com.sinergiadigital.handyman.GPS.BusManager;

public class AppHandyMan extends Application {
    ApplicationPresenter mApplicationPresenter;
    public static String secretKey;

    @Override
    public void onCreate() {
        super.onCreate();
        BusManager.init();
        mApplicationPresenter = new ApplicationPresenter(this);
        mApplicationPresenter.setup();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
