package com.sinergiadigital.handyman.Model;

public class Material {
    public String mMaterialName;
    public String mQuantity;

    public Material(String mMaterialName, String mQuantity) {
        this.mMaterialName = mMaterialName;
        this.mQuantity = mQuantity;
    }
}
