package com.sinergiadigital.handyman.Model;

import android.location.Location;

public class LocationRetrieve {
    public Location location;

    public LocationRetrieve(Location location) {
        this.location = location;
    }

    public Location getLocation(){
        return  location;
    }
}
