package com.sinergiadigital.handyman.Model;

public class ServiceHandy {
    public String mDateService;
    public String mUser;
    public String mAddress;
    public String mPhone;
    public String mTagUser;
    public String mCancelService;


    public ServiceHandy(String mDateService, String mUser, String mAddress) {
        this.mDateService = mDateService;
        this.mUser = mUser;
        this.mAddress = mAddress;
    }

    public ServiceHandy(String mDateService, String mUser, String mAddress, String mPhone, String mTagUser, String mCancelService) {
        this.mDateService = mDateService;
        this.mUser = mUser;
        this.mAddress = mAddress;
        this.mPhone = mPhone;
        this.mTagUser = mTagUser;
        this.mCancelService = mCancelService;
    }
}
