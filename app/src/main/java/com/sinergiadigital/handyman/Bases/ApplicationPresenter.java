package com.sinergiadigital.handyman.Bases;

import android.content.Context;

import com.sinergiadigital.handyman.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationPresenter{
    private Context context;

    public ApplicationPresenter(Context context) {
        this.context = context;
    }

    public void setup(){
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/GothamMedium.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
