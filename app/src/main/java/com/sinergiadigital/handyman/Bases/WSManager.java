package com.sinergiadigital.handyman.Bases;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class WSManager extends BaseWSManager {

    public static boolean DEBUG_ENABLED = false;

    public static WSManager init() {
        return new WSManager();
    }

    public class WS {
        public static final String LOGIN = "login";
    }

    @Override
    protected Call<ResponseBody> getWebService(String webServiceValue, WSBaseRequestInterface wsBaseRequest) {
        Call<ResponseBody> call = null;
        /*TaxsiAppDefinitions taxiWebServicesDefinition = WebServices.taxsiAppDefinitions();
        switch (webServiceValue) {
            case WS.LOGIN:
                LoginRequest loginRequest = (LoginRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.login(loginRequest.email,loginRequest.password);
                break;
        }*/
        return call;
    }

    @Override
    protected Call<ResponseBody> getQueryWebService(String webServiceValue, String requestValue) {
        return null;
    }

    @Override
    protected String getJsonDebug(String requestUrl) {
        return null;
    }

    @Override
    protected boolean getErrorDebugEnabled() {
        return false;
    }

    @Override
    protected boolean getDebugEnabled() {
        return false;
    }
}

