package com.sinergiadigital.handyman.Modules.MainMenu.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sinergiadigital.handyman.Model.ServiceHandy;
import com.sinergiadigital.handyman.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {
    private List<ServiceHandy> listHistory = new ArrayList<>();
    private OnGetInfoServiceCallback mOnGetInfoServiceCallback;

    public HistoryAdapter(OnGetInfoServiceCallback mOnGetInfoServiceCallback) {
        this.mOnGetInfoServiceCallback = mOnGetInfoServiceCallback;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        HistoryHolder vHolder = new HistoryHolder(layoutView);
        return vHolder;
    }

    public void update(List<ServiceHandy> listHistory){
        this.listHistory = listHistory;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        holder.mDateServiceTextView.setText(listHistory.get(position).mDateService);
        holder.mDateServiceTextView.setText(listHistory.get(position).mUser);
        holder.mAddressTextView.setText(listHistory.get(position).mAddress);
    }

    @Override
    public int getItemCount() {
        return listHistory.size();
    }
    public class HistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mDateServiceTextView;
        private TextView mUserTextView;
        private TextView mAddressTextView;
        private Button mViewMoreButton;

        public HistoryHolder(View itemView) {
            super(itemView);
            mDateServiceTextView = itemView.findViewById(R.id.mDateServiceTextView);
            mUserTextView = itemView.findViewById(R.id.mUserTextView);
            mAddressTextView = itemView.findViewById(R.id.mAddressTextView);
            mViewMoreButton = itemView.findViewById(R.id.mViewMoreButton);
            mViewMoreButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mOnGetInfoServiceCallback.OnGetDetailService(HistoryHolder.this);
        }
    }

    public interface OnGetInfoServiceCallback{
        void OnGetDetailService(HistoryHolder vh);
    }
}




