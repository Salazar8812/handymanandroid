package com.sinergiadigital.handyman.Modules.MainMenu.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sinergiadigital.handyman.Bases.BaseFragment;
import com.sinergiadigital.handyman.Model.ServiceHandy;
import com.sinergiadigital.handyman.Modules.MainMenu.Adapters.NextAdapter;
import com.sinergiadigital.handyman.Modules.MainMenu.Adapters.NextHolder;
import com.sinergiadigital.handyman.Modules.MainMenu.DetailServiceActivity;
import com.sinergiadigital.handyman.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NextFragment extends BaseFragment  {
    @BindView(R.id.mListNextRecyclerView)
    public RecyclerView mListNextRecyclerView;
    private NextAdapter mNextAdapter;
    private List<ServiceHandy> mListNext = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.next_fragment, container, false);
        ButterKnife.bind(this,v);
        populateNextService();
        configureRecyclerView();
        return v;
    }

    private void populateNextService(){
        mListNext.add(new ServiceHandy("Martes 20 de noviembre, 11:20 AM","Juan Perez","Av. Ruiz Cortines 1402","1238872767","pintor",""));
        mListNext.add(new ServiceHandy("Viernes 20 de febrero, 11:20 AM","José Garcia","Av. Cicuito Presidentes 8029","1238872767","pintor",""));
        mListNext.add(new ServiceHandy("Lunes 20 de diciembre, 11:20 AM","Miguel Sosa","Av. Lazaro Cardenas 7622","1238872767","pintor",""));
        mListNext.add(new ServiceHandy("Jueves 20 de marzo, 11:20 AM","Pedro Jimenez","Av. Xalapa 2301","1238872767","pintor",""));
        mListNext.add(new ServiceHandy("Miercoles 20 de abril, 11:20 AM","Manuel Gomez","Av. 20 de noviembre 2012","1238872767","pintor",""));
    }

    private void configureRecyclerView(){
        mNextAdapter = new NextAdapter();
        mListNextRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mListNextRecyclerView.setAdapter(mNextAdapter);
        mNextAdapter.update(mListNext);
    }
}
