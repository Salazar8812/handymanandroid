package com.sinergiadigital.handyman.Modules.MainMenu.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinergiadigital.handyman.Model.ServiceHandy;
import com.sinergiadigital.handyman.R;

public class NextHolder extends RecyclerView.ViewHolder {
    private TextView mDateSeriveTextView;
    private TextView mTagProfileUserTextView;
    private TextView mUserTextView;
    private TextView mAddressTextView;
    private TextView mPhoneTextView;
    private Button mAcceptButton;
    private Button mCancelButton;
    private LinearLayout mContentOptionServiceLinearLayout;
    private TextView mTagCancelServiceTextView;
    private ServiceHandy mServiceHandy;
    private OnItemSelectChange mOnItemSelectChange;
    private LinearLayout mContentTagCancelLinearLayout;



    public NextHolder(@NonNull View itemView, OnItemSelectChange onItemSelectChange) {
        super(itemView);
        mOnItemSelectChange = onItemSelectChange;
        mDateSeriveTextView = itemView.findViewById(R.id.mDateServiceTextView);
        mTagProfileUserTextView = itemView.findViewById(R.id.mTagProfileUserTextView);
        mUserTextView = itemView.findViewById(R.id.mUserTextView);
        mAddressTextView = itemView.findViewById(R.id.mAddressTextView);
        mPhoneTextView = itemView.findViewById(R.id.mPhoneTextView);
        mAcceptButton = itemView.findViewById(R.id.mAcceptServiceButton);
        mCancelButton = itemView.findViewById(R.id.mCancelServiceButton);
        mContentOptionServiceLinearLayout = itemView.findViewById(R.id.mContentOptionServiceLinearLayout);
        mTagCancelServiceTextView = itemView.findViewById(R.id.mTagCancelServiceTextView);
        mContentTagCancelLinearLayout = itemView.findViewById(R.id.mContentTagCancelLinearLayout);
    }

    public void render(@NonNull ServiceHandy mServiceHandy, @NonNull String selection) {
        this.mServiceHandy = mServiceHandy;
        mDateSeriveTextView.setText(mServiceHandy.mDateService);
        mTagProfileUserTextView.setText(mServiceHandy.mTagUser);
        mUserTextView.setText(mServiceHandy.mUser);
        mAddressTextView.setText(mServiceHandy.mAddress);
        mPhoneTextView.setText(mServiceHandy.mPhone);

        if(mServiceHandy.mCancelService.equals("true")) {
            mContentOptionServiceLinearLayout.setBackgroundColor(Color.parseColor("#FFE4E4E4"));
            mContentTagCancelLinearLayout.setVisibility(View.VISIBLE);
            mAcceptButton.setVisibility(View.GONE);
            mCancelButton.setVisibility(View.GONE);
        }else if(mServiceHandy.mCancelService.equals("false")){
            mContentOptionServiceLinearLayout.setBackgroundColor(Color.parseColor("#D41D1A"));
            mContentTagCancelLinearLayout.setVisibility(View.VISIBLE);
            mTagCancelServiceTextView.setText("SERVICIO ACEPTADO");
            mTagCancelServiceTextView.setTextColor(Color.parseColor("#ffffff"));
            mAcceptButton.setVisibility(View.GONE);
            mCancelButton.setVisibility(View.GONE);
        }

        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                mOnItemSelectChange.OnSelectItem(NextHolder.this,"false");
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemSelectChange.OnSelectItem(NextHolder.this,"true");
            }
        });
    }


    public ServiceHandy getService() {
        return mServiceHandy;
    }

    interface OnItemSelectChange{
        void OnSelectItem(NextHolder vh,String selection);
    }
}
