package com.sinergiadigital.handyman.Modules.MainMenu.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sinergiadigital.handyman.Bases.BaseFragment;
import com.sinergiadigital.handyman.GPS.BusManager;
import com.sinergiadigital.handyman.Model.LocationRetrieve;
import com.sinergiadigital.handyman.Modules.MainMenu.DetailServiceActivity;
import com.sinergiadigital.handyman.Modules.MainMenu.Dialogs.DialogRequestService;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.DrawMarker;
import com.sinergiadigital.handyman.Utils.DrawRouteMaps;
import com.sinergiadigital.handyman.Utils.InternalData;
import com.sinergiadigital.handyman.Utils.StringKeys;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodayFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener{
    private GoogleMap mGoogleMap;
    private Marker mk = null;
    private InternalData mInternalData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.today_fragment, container, false);
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
        mInternalData = new InternalData(getActivity());
        startActivityForResult(new Intent(getActivity(), DialogRequestService.class),1);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,getActivity());
    }

    @OnClick(R.id.mMoreDetailContentRelative)
    public void OnClickDetail(){
        startActivity(new Intent(getActivity(), DetailServiceActivity.class));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        drawRoute(new LatLng(Double.parseDouble(mInternalData.getData("latitude")), Double.parseDouble(mInternalData.getData("longitude"))),new LatLng(StringKeys.LATITUD,StringKeys.LONGITUD));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnLocationRetrieve(final LocationRetrieve eventLocation) {

    }

    public void drawRoute(LatLng mFrom, LatLng mArrive){
        try {
            LatLng origin = mFrom;
            LatLng destination = new LatLng(mArrive.latitude, mArrive.longitude);

            mGoogleMap.clear();
                DrawRouteMaps.getInstance(getActivity())
                        .draw(origin, destination, mGoogleMap);

                DrawMarker.getInstance(getActivity()).drawOrigin(mGoogleMap,mFrom,R.drawable.ic_pin, "Mi Ubicación");
                DrawMarker.getInstance(getActivity()).drawDestination(mGoogleMap, destination, R.drawable.ic_black_circle, "Destino");

                LatLngBounds bounds = new LatLngBounds.Builder()
                        .include(origin)
                        .include(destination).build();
                Point displaySize = new Point();
                getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));

        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            BusManager.register(this);
        }catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    public void createMarker(Location driverLocation){
        mGoogleMap.clear();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        try {
            int height = 120;
            int width = 120;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_pin);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            mk = mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()))
                    .title("")
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))));

            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()), 16.5f));

        }catch (NullPointerException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        createMarker(generateLocationFromInternalData(Double.parseDouble(mInternalData.getData("latitude")),Double.parseDouble(mInternalData.getData("longitude"))));
    }

    Location generateLocationFromInternalData(double latitude, double longitude) {
        Location location = new Location("handyDummyLocation");
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        return location;
    }
}
