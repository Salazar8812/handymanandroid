package com.sinergiadigital.handyman.Modules.Login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;

import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.GPS.GPSClass;
import com.sinergiadigital.handyman.Modules.MainMenu.MainMenuActivity;
import com.sinergiadigital.handyman.Modules.Register.RegisterActivity;
import com.sinergiadigital.handyman.Modules.RestorePassword.RestorePasswordActivity;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.InternalData;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {
    private LocationManager locationManager;
    private InternalData mInternalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        requestPermission();
        mInternalData = new InternalData(this);
;    }

    @OnClick({R.id.mEnterButton,R.id.mRegisterButton,R.id.mForgotPasswordButton})
    public void OnClickEnter(View v){
        switch (v.getId()){
            case R.id.mEnterButton:
                startActivity(new Intent(this, MainMenuActivity.class));
                break;
            case R.id.mRegisterButton:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
            case R.id.mForgotPasswordButton:
                startActivity(new Intent(this, RestorePasswordActivity.class));
                break;
                default:break;
        }
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1); //Any number can be used    }
        }else{
            gpsCheckStatus();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        starLocation();
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }

    public void starLocation(){
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mInternalData.saveData("latitude",latitude);
                        mInternalData.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );

        initServiceGPS();
    }

    private void initServiceGPS(){
        Intent i =new Intent(getApplicationContext(),GPSClass.class);
        startService(i);
    }
}
