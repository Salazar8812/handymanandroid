package com.sinergiadigital.handyman.Modules.MainMenu.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.SupportMapFragment;
import com.sinergiadigital.handyman.Bases.BaseFragment;
import com.sinergiadigital.handyman.Model.ServiceHandy;
import com.sinergiadigital.handyman.Modules.MainMenu.Adapters.HistoryAdapter;
import com.sinergiadigital.handyman.Modules.MainMenu.DetailServiceActivity;
import com.sinergiadigital.handyman.Modules.MainMenu.DetailServiceDoneActivity;
import com.sinergiadigital.handyman.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryFragment extends BaseFragment implements HistoryAdapter.OnGetInfoServiceCallback {
    @BindView(R.id.mListHistory)
    RecyclerView mListHistoryRecyclerView;

    private HistoryAdapter mHistoryAdapter;
    private List<ServiceHandy> mListServiceHistory = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.history_fragment, container, false);
        ButterKnife.bind(this,v);

        configureAdapter();
        populateHistoryDummy();

        return v;
    }

    private void configureAdapter(){
        mListHistoryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mHistoryAdapter = new HistoryAdapter(this);
        mListHistoryRecyclerView.setAdapter(mHistoryAdapter);
        mHistoryAdapter.update(mListServiceHistory);
    }

    private void populateHistoryDummy(){
        mListServiceHistory.add(new ServiceHandy("Martes 20 de noviembre, 11:20 AM","Juan Perez","Av. Ruiz Cortines 1402"));
        mListServiceHistory.add(new ServiceHandy("Viernes 20 de febrero, 11:20 AM","José Garcia","Av. Cicuito Presidentes 8029"));
        mListServiceHistory.add(new ServiceHandy("Lunes 20 de diciembre, 11:20 AM","Miguel Sosa","Av. Lazaro Cardenas 7622"));
        mListServiceHistory.add(new ServiceHandy("Jueves 20 de marzo, 11:20 AM","Pedro Jimenez","Av. Xalapa 2301"));
        mListServiceHistory.add(new ServiceHandy("Miercoles 20 de abril, 11:20 AM","Manuel Gomez","Av. 20 de noviembre 2012"));
        mListServiceHistory.add(new ServiceHandy("Martes 20 de mayo, 11:20 AM","Enrique Hernández","Av. Aztecas 1233"));
    }

    @Override
    public void OnGetDetailService(HistoryAdapter.HistoryHolder vh) {
        startActivity(new Intent(getActivity(), DetailServiceDoneActivity.class));
    }
}
