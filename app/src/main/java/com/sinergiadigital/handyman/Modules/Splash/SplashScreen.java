package com.sinergiadigital.handyman.Modules.Splash;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.sinergiadigital.handyman.GPS.GPSClass;
import com.sinergiadigital.handyman.Modules.Login.LoginActivity;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.InternalData;

public class SplashScreen extends AppCompatActivity {
    private LocationManager locationManager;
    private InternalData mInternalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        mInternalData = new InternalData(this);
        gpsCheckStatus();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }

    public void starLocation(){
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mInternalData.saveData("latitude",latitude);
                        mInternalData.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );

        initServiceGPS();
    }

    private void initServiceGPS(){
        Intent i =new Intent(getApplicationContext(),GPSClass.class);
        startService(i);
    }
}
