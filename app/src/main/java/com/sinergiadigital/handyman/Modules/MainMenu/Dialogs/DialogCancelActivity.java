package com.sinergiadigital.handyman.Modules.MainMenu.Dialogs;

import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogCancelActivity extends BaseActivity {
    @BindView(R.id.mCancelButton)
    Button mCancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_cancel_service);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mCancelButton)
    public void OnClickCancelButton(){
        finish();
    }

    @OnClick(R.id.mAccepConfirmButton)
    public void OnClickConfirmbutton(){
        finish();
    }


}
