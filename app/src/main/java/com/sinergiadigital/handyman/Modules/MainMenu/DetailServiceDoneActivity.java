package com.sinergiadigital.handyman.Modules.MainMenu;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;

import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.DrawMarker;
import com.sinergiadigital.handyman.Utils.DrawRouteMaps;
import com.sinergiadigital.handyman.Utils.InternalData;
import com.sinergiadigital.handyman.Utils.StringKeys;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailServiceDoneActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.mSwitch)
    LabeledSwitch mSwitch;
    private GoogleMap mGoogleMap;
    private Marker mk = null;
    private InternalData mInternalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_service_done_activity);
        ButterKnife.bind(this);
        mInternalData = new InternalData(this);
        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(
                this);
        addListenerSwitch();
    }


    @OnClick({R.id.mListButton})
    public void OnClickList(View view){
        startActivity(new Intent(this, MaterialListActivity.class));
    }

    private void configureSwitch(){
        Typeface mGothamFont = Typeface.createFromAsset(getAssets(),
                "fonts/GothamMedium.ttf");
        mSwitch.setColorOn(Color.parseColor("#D41D1A"));
        mSwitch.setColorOff(Color.parseColor("#ffffff"));
        mSwitch.setTypeface(mGothamFont);
        mSwitch.setLabelOn("on");
        mSwitch.setLabelOff("off");
    }

    private void addListenerSwitch(){
        mSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if(isOn){
                    mSwitch.setColorOn(Color.parseColor("#ffffff"));
                    mSwitch.setColorOff(Color.parseColor("#000000"));
                }else{
                    mSwitch.setColorOn(Color.parseColor("#D41D1A"));
                    mSwitch.setColorOff(Color.parseColor("#ffffff"));
                }
            }
        });
    }

    public void drawRoute(LatLng mFrom, LatLng mArrive){
        try {
            LatLng origin = mFrom;
            LatLng destination = new LatLng(mArrive.latitude, mArrive.longitude);

            mGoogleMap.clear();
            DrawRouteMaps.getInstance(this)
                    .draw(origin, destination, mGoogleMap);

            DrawMarker.getInstance(this).drawOrigin(mGoogleMap,mFrom,R.drawable.ic_pin, "Mi Ubicación");
            DrawMarker.getInstance(this).drawDestination(mGoogleMap, destination, R.drawable.ic_black_circle, "Destino");

            LatLngBounds bounds = new LatLngBounds.Builder()
                    .include(origin)
                    .include(destination).build();
            Point displaySize = new Point();
            this.getWindowManager().getDefaultDisplay().getSize(displaySize);

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));

        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        drawRoute(new LatLng(Double.parseDouble(mInternalData.getData("latitude")), Double.parseDouble(mInternalData.getData("longitude"))),new LatLng(StringKeys.LATITUD,StringKeys.LONGITUD));
    }
}
