package com.sinergiadigital.handyman.Modules.MainMenu.Dialogs;

import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogRequestService extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_request_service);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.mAcceptServiceButton, R.id.mRefusedServiceButton})
    public void OnClickButton(View v){
        switch (v.getId()){
            case R.id.mAcceptServiceButton:
                setResult(1);
                break;
            case R.id.mRefusedServiceButton:
                setResult(2);
                break;
                default:break;
        }
        finish();
    }
}
