package com.sinergiadigital.handyman.Modules.MainMenu.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sinergiadigital.handyman.Model.Material;
import com.sinergiadigital.handyman.R;

import java.util.ArrayList;
import java.util.List;

public class ListMaterialAdapter extends RecyclerView.Adapter<ListMaterialAdapter.MaterialHolder> {
    private List<Material> listHistory = new ArrayList<>();
    private HistoryAdapter.OnGetInfoServiceCallback mOnGetInfoServiceCallback;

    public ListMaterialAdapter() {
    }

    @Override
    public ListMaterialAdapter.MaterialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_material, parent, false);
        ListMaterialAdapter.MaterialHolder vHolder = new ListMaterialAdapter.MaterialHolder(layoutView);
        return vHolder;
    }

    public void update(List<Material> listHistory) {
        this.listHistory = listHistory;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ListMaterialAdapter.MaterialHolder holder, int position) {
        holder.mNameMaterial.setText(listHistory.get(position).mMaterialName);
        holder.mQuantity.setText(listHistory.get(position).mQuantity);
    }

    @Override
    public int getItemCount() {
        return listHistory.size();
    }

    public class MaterialHolder extends RecyclerView.ViewHolder{
        private TextView mNameMaterial;
        private TextView mQuantity;


        public MaterialHolder(View itemView) {
            super(itemView);
            mNameMaterial = itemView.findViewById(R.id.mMaterialNameTextView);
            mQuantity = itemView.findViewById(R.id.mQuantityTextView);
        }

    }
}