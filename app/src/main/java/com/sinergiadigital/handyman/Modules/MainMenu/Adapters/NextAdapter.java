package com.sinergiadigital.handyman.Modules.MainMenu.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.sinergiadigital.handyman.Model.ServiceHandy;
import com.sinergiadigital.handyman.R;

import java.util.ArrayList;
import java.util.List;

public class NextAdapter extends RecyclerView.Adapter<NextHolder> implements NextHolder.OnItemSelectChange {
    private List<ServiceHandy> mListPrediction = new ArrayList<>();
    private List<NextHolder> mViewHolders = new ArrayList<>();

    public NextAdapter() {

    }

    @Override
    public NextHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return insertViewHolder(new NextHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_next,
                        parent, false),this));
    }

    private NextHolder insertViewHolder(@NonNull NextHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    public void update(List<ServiceHandy> mListPrediction){
        this.mListPrediction = mListPrediction;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(NextHolder holder, int position) {
        holder.render(mListPrediction.get(position), "");
    }

    @Override
    public int getItemCount() {
        return mListPrediction.size();
    }

    @Override
    public void OnSelectItem(NextHolder vh, String mSelected) {
        ServiceHandy client = vh.getService();
        client.mCancelService = mSelected;
        // Iterate through the list of ViewHolders.
        for (NextHolder cVh : mViewHolders) {
            // Get the current ViewHolder client object.
            ServiceHandy mServiceHandy = cVh.getService();
            // Check if the ViewHolder is the same as the one reporting the change.
            if (client.equals(mServiceHandy)) {
                // If it is, set the new selection change to said ViewHolder.
                cVh.render(mServiceHandy, mSelected);
            } else {
                // If not, deselect the ViewHolder.
                cVh.render(mServiceHandy, mSelected);
            }
        }
    }
}
