package com.sinergiadigital.handyman.Modules.MainMenu;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.Model.Material;
import com.sinergiadigital.handyman.Modules.MainMenu.Adapters.ListMaterialAdapter;
import com.sinergiadigital.handyman.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MaterialListActivity extends BaseActivity {
    @BindView(R.id.mSwitch)
    LabeledSwitch mSwitch;
    @BindView(R.id.mListMaterialRecyclerView)
    RecyclerView mListMaterialRecyclerView;
    private ListMaterialAdapter mListMaterialAdapter;
    private List<Material> mListMaterial = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.material_list_activity);
        ButterKnife.bind(this);
        populateListMaterial();
        configureRecyclerView();
        configureRecyclerView();
        addListenerSwitch();
    }

    private void configureSwitch(){
        Typeface mGothamFont = Typeface.createFromAsset(getAssets(),
                "fonts/GothamMedium.ttf");
        mSwitch.setColorOn(Color.parseColor("#D41D1A"));
        mSwitch.setColorOff(Color.parseColor("#ffffff"));
        mSwitch.setTypeface(mGothamFont);
        mSwitch.setLabelOn("on");
        mSwitch.setLabelOff("off");
    }

    private void addListenerSwitch(){
        mSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if(isOn){
                    mSwitch.setColorOn(Color.parseColor("#ffffff"));
                    mSwitch.setColorOff(Color.parseColor("#000000"));
                }else{
                    mSwitch.setColorOn(Color.parseColor("#D41D1A"));
                    mSwitch.setColorOff(Color.parseColor("#ffffff"));
                }
            }
        });
    }

    private void configureRecyclerView(){
        mListMaterialRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mListMaterialAdapter = new ListMaterialAdapter();
        mListMaterialRecyclerView.setAdapter(mListMaterialAdapter);
        mListMaterialAdapter.update(mListMaterial);
    }

    private void populateListMaterial(){
        mListMaterial.add(new Material("Pintura blanca a la cal en polvo","10L"));
        mListMaterial.add(new Material("Pegamento impermeable","2L"));
        mListMaterial.add(new Material("Tanque de agua tricapa","2"));
    }

}
