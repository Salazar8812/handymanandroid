package com.sinergiadigital.handyman.Modules.MainMenu;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Button;

import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.Modules.MainMenu.Dialogs.DialogCancelActivity;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.DrawMarker;
import com.sinergiadigital.handyman.Utils.DrawRouteMaps;
import com.sinergiadigital.handyman.Utils.InternalData;
import com.sinergiadigital.handyman.Utils.StringKeys;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailServiceActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.mSwitch)
    LabeledSwitch mSwitch;
    @BindView(R.id.mCancelServiceButton)
    Button mCancelServiceButton;

    private GoogleMap mGoogleMap;
    private Marker mk = null;
    private InternalData mInternalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_service_activity);
        ButterKnife.bind(this);
        mInternalData = new InternalData(this);
        SupportMapFragment mapFrag = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(
                this);
        configureSwitch();
        addListenerSwitch();
    }

    @OnClick(R.id.mCancelServiceButton)
    public void OnClickCancel(){
        startActivity(new Intent(this, DialogCancelActivity.class));
    }

    private void addListenerSwitch(){
        mSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if(isOn){
                    mSwitch.setColorOn(Color.parseColor("#ffffff"));
                    mSwitch.setColorOff(Color.parseColor("#000000"));
                }else{
                    mSwitch.setColorOn(Color.parseColor("#D41D1A"));
                    mSwitch.setColorOff(Color.parseColor("#ffffff"));
                }
            }
        });
    }

    public void drawRoute(LatLng mFrom, LatLng mArrive){
        try {
            LatLng origin = mFrom;
            LatLng destination = new LatLng(mArrive.latitude, mArrive.longitude);

            mGoogleMap.clear();
            DrawRouteMaps.getInstance(this)
                    .draw(origin, destination, mGoogleMap);

            DrawMarker.getInstance(this).drawOrigin(mGoogleMap,mFrom,R.drawable.ic_pin, "Mi Ubicación");
            DrawMarker.getInstance(this).drawDestination(mGoogleMap, destination, R.drawable.ic_black_circle, "Destino");

            LatLngBounds bounds = new LatLngBounds.Builder()
                    .include(origin)
                    .include(destination).build();
            Point displaySize = new Point();
            this.getWindowManager().getDefaultDisplay().getSize(displaySize);

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));

        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    private void configureSwitch(){
        Typeface mGothamFont = Typeface.createFromAsset(getAssets(),
                "fonts/GothamMedium.ttf");
        mSwitch.setColorOn(Color.parseColor("#D41D1A"));
        mSwitch.setColorOff(Color.parseColor("#ffffff"));
        mSwitch.setTypeface(mGothamFont);
        mSwitch.setLabelOn("on");
        mSwitch.setLabelOff("off");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        createMarker(generateLocationFromInternalData(Double.parseDouble(mInternalData.getData("latitude")),Double.parseDouble(mInternalData.getData("longitude"))));
        drawRoute(new LatLng(Double.parseDouble(mInternalData.getData("latitude")), Double.parseDouble(mInternalData.getData("longitude"))),new LatLng(StringKeys.LATITUD,StringKeys.LONGITUD));
    }

    public void createMarker(Location driverLocation){
        mGoogleMap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        try {
            int height = 120;
            int width = 120;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_pin);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            mk = mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()))
                    .title("")
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))));

            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverLocation.getLatitude(),driverLocation.getLongitude()), 16.5f));

        }catch (NullPointerException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    Location generateLocationFromInternalData(double latitude, double longitude) {
        Location location = new Location("handyDummyLocation");
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        return location;
    }
}
