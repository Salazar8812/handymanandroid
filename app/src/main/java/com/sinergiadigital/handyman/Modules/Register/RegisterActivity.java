package com.sinergiadigital.handyman.Modules.Register;

import android.os.Bundle;

import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.R;

import butterknife.ButterKnife;

public class RegisterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        ButterKnife.bind(this);
    }
}
