package com.sinergiadigital.handyman.Modules.MainMenu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.Modules.MainMenu.Adapters.PageOptionAdapter;
import com.sinergiadigital.handyman.Modules.MainMenu.Dialogs.DialogRequestService;
import com.sinergiadigital.handyman.Modules.MainMenu.Fragments.HistoryFragment;
import com.sinergiadigital.handyman.Modules.MainMenu.Fragments.NextFragment;
import com.sinergiadigital.handyman.Modules.MainMenu.Fragments.TodayFragment;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.ViewPagerUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainMenuActivity extends BaseActivity {
    @BindView(R.id.mPager)
    ViewPagerUtils mPager;
    @BindView(R.id.mOptionTabs)
    TabLayout mTabs;
    @BindView(R.id.mSwitch)
    LabeledSwitch mSwitch;
    private PageOptionAdapter mPageOptionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);
        ButterKnife.bind(this);

        setupViewPager(mPager);

        mTabs.setupWithViewPager(mPager);
        mPager.setPagingEnabled(false);

        setupTabIcons();
        setColorIconTabs();
        setColorFirstOption();

        configureSwitch();
        addListenerSwitch();

    }

    private void addListenerSwitch(){
        mSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if(isOn){
                    mSwitch.setColorOn(Color.parseColor("#ffffff"));
                    mSwitch.setColorOff(Color.parseColor("#000000"));
                }else{
                    mSwitch.setColorOn(Color.parseColor("#D41D1A"));
                    mSwitch.setColorOff(Color.parseColor("#ffffff"));
                }
            }
        });
    }

    private void configureSwitch(){
        Typeface mGothamFont = Typeface.createFromAsset(getAssets(),
                "fonts/GothamMedium.ttf");
        mSwitch.setColorOn(Color.parseColor("#D41D1A"));
        mSwitch.setColorOff(Color.parseColor("#ffffff"));
        mSwitch.setTypeface(mGothamFont);
        mSwitch.setLabelOn("on");
        mSwitch.setLabelOff("off");
    }

    public void setColorFirstOption(){
        mTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(MainMenuActivity.this, R.color.handy_color_red);
                ImageView img_icon = (ImageView)tab.getCustomView().findViewById(R.id.iconTab);
                img_icon.getBackground().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                TextView text_icon = (TextView) tab.getCustomView().findViewById(R.id.text_tab);
                text_icon.setTextColor(getResources().getColor(R.color.handy_color_red));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(MainMenuActivity.this, R.color.handy_color_gray);
                ImageView img_icon = (ImageView)tab.getCustomView().findViewById(R.id.iconTab);
                img_icon.getBackground().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

                TextView text_icon = (TextView) tab.getCustomView().findViewById(R.id.text_tab);
                text_icon.setTextColor(getResources().getColor(R.color.handy_color_gray));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        mPageOptionAdapter = new PageOptionAdapter(getSupportFragmentManager());
        mPageOptionAdapter.addFragment(new TodayFragment(), "Hoy");
        mPageOptionAdapter.addFragment(new NextFragment(), "Próximos");
        mPageOptionAdapter.addFragment(new HistoryFragment(), "Historial");
        viewPager.setAdapter(mPageOptionAdapter);
    }

    private void setupTabIcons() {
        LayoutInflater inflater = (LayoutInflater) getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        View customtab = inflater.inflate(R.layout.custom_option_tab, null);
        ImageView icon_tab = (ImageView) customtab.findViewById(R.id.iconTab);
        TextView text_tab= (TextView)customtab.findViewById(R.id.text_tab);
        RelativeLayout mBadge = customtab.findViewById(R.id.mContentBadge);
        TextView mBadgeNotification = customtab.findViewById(R.id.mBadgeTextView);

        customtab = inflater.inflate(R.layout.custom_option_tab, null);
        icon_tab = (ImageView) customtab.findViewById(R.id.iconTab);
        text_tab= (TextView)customtab.findViewById(R.id.text_tab);
        mBadge = customtab.findViewById(R.id.mContentBadge);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            icon_tab.setBackgroundDrawable(getResources().getDrawable(R.drawable._inicio_one));
            text_tab.setText("Hoy");
            mBadge.setVisibility(View.GONE);
        }else {
            icon_tab.setBackground(getResources().getDrawable(R.drawable._inicio_one));
            text_tab.setText("Hoy");
            mBadge.setVisibility(View.GONE);

        }
        mTabs.getTabAt(0).setCustomView(customtab);

        customtab = inflater.inflate(R.layout.custom_option_tab, null);
        icon_tab = (ImageView) customtab.findViewById(R.id.iconTab);
        text_tab= (TextView)customtab.findViewById(R.id.text_tab);
        mBadge = customtab.findViewById(R.id.mContentBadge);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            icon_tab.setBackgroundDrawable(getResources().getDrawable(R.drawable.servicios_one));
            text_tab.setText("Próximos");
            mBadge.setVisibility(View.VISIBLE);
        }else {
            icon_tab.setBackground(getResources().getDrawable(R.drawable.servicios_one));
            text_tab.setText("Próximos");
            mBadge.setVisibility(View.VISIBLE);

        }
        mTabs.getTabAt(1).setCustomView(customtab);


        customtab = inflater.inflate(R.layout.custom_option_tab, null);
        icon_tab = (ImageView) customtab.findViewById(R.id.iconTab);
        text_tab= (TextView)customtab.findViewById(R.id.text_tab);
        mBadge = customtab.findViewById(R.id.mContentBadge);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            icon_tab.setBackgroundDrawable(getResources().getDrawable(R.drawable._costo_one));
            text_tab.setText("Historial");
            mBadge.setVisibility(View.GONE);

        }else {
            icon_tab.setBackground(getResources().getDrawable(R.drawable._costo_one));
            text_tab.setText("Historial");
            mBadge.setVisibility(View.GONE);

        }
        mTabs.getTabAt(2).setCustomView(customtab);

    }

    public void setColorIconTabs(){
        int tabIconColor = ContextCompat.getColor(this, R.color.handy_color_red);
        ImageView img_icon = (ImageView)mTabs.getTabAt(0).getCustomView().findViewById(R.id.iconTab);
        img_icon.getBackground().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        TextView text_icon = (TextView) mTabs.getTabAt(0).getCustomView().findViewById(R.id.text_tab);
        text_icon.setTextColor(getResources().getColor(R.color.handy_color_red));

        int tabIconColor2 = ContextCompat.getColor(this, R.color.handy_color_gray);
        ImageView img_icon2 = (ImageView)mTabs.getTabAt(1).getCustomView().findViewById(R.id.iconTab);
        img_icon2.getBackground().setColorFilter(tabIconColor2, PorterDuff.Mode.SRC_IN);

        TextView text_icon2 = (TextView) mTabs.getTabAt(1).getCustomView().findViewById(R.id.text_tab);
        text_icon2.setTextColor(getResources().getColor(R.color.handy_color_gray));

        int tabIconColor3 = ContextCompat.getColor(this, R.color.handy_color_gray);
        ImageView img_icon3 = (ImageView)mTabs.getTabAt(2).getCustomView().findViewById(R.id.iconTab);
        img_icon3.getBackground().setColorFilter(tabIconColor3, PorterDuff.Mode.SRC_IN);

        TextView text_icon3 = (TextView) mTabs.getTabAt(2).getCustomView().findViewById(R.id.text_tab);
        text_icon3.setTextColor(getResources().getColor(R.color.handy_color_gray));

    }
}
