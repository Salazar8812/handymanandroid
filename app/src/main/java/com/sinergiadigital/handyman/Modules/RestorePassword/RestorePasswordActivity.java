package com.sinergiadigital.handyman.Modules.RestorePassword;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.sinergiadigital.handyman.Bases.BaseActivity;
import com.sinergiadigital.handyman.Modules.MainMenu.Dialogs.DialogErrorRestorePass;
import com.sinergiadigital.handyman.Modules.MainMenu.Dialogs.DialogSuccessRestorePass;
import com.sinergiadigital.handyman.R;
import com.sinergiadigital.handyman.Utils.MessageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestorePasswordActivity extends BaseActivity {

    @BindView(R.id.mNewPassword)
    EditText mNewPasword;
    @BindView(R.id.mConfirmNewPassword)
    EditText mConfirmNewPassword;

    private String newPassword;
    private String confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restore_password_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.mSaveNewPassword)
    public void onClickSavePassword() {
        newPassword = mNewPasword.getText().toString().trim();
        confirmPassword = mConfirmNewPassword.getText().toString().trim();
        if (!newPassword.isEmpty() && !confirmPassword.isEmpty()) {
            if (newPassword.equals(confirmPassword)) {
                startActivity(new Intent(this, DialogSuccessRestorePass.class));
            } else {
                startActivity(new Intent(this, DialogErrorRestorePass.class));
            }
        } else {
            MessageUtils.toast(this, "Ingresa usuario y contraseña");
        }
    }
}
