package com.sinergiadigital.handyman.GPS;

import org.greenrobot.eventbus.EventBus;

public class BusManager {

    private static EventBus eventBus;

    public BusManager() {
        eventBus = EventBus.getDefault();
    }

    public static void init() {
        eventBus = new EventBus();
    }

    public static void register(Object o) {
        checkIfValid();
        eventBus.register(o);
    }

    private static void checkIfValid() {
        if (eventBus == null) throw new IllegalStateException("Call BusManager.init() first");
    }

    public static void unregister(Object o) {
        checkIfValid();
        eventBus.unregister(o);
    }

    public static void post(Object o) {
        eventBus.post(o);
    }
}