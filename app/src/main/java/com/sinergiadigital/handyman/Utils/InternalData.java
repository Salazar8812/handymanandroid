package com.sinergiadigital.handyman.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class InternalData {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public InternalData(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void saveData(String key,String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void removeData(String key){
        editor.remove(key).commit();
    }

    public String getData(String key){
        return sharedPreferences.getString(key, "");
    }

}
