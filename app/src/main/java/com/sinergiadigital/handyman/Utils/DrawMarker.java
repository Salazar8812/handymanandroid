package com.sinergiadigital.handyman.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by ocittwo on 11/14/16.
 *
 * @Author Ahmad Rosid
 * @Email ocittwo@gmail.com
 * @Github https://github.com/ar-android
 * @Web http://ahmadrosid.com
 */
public class DrawMarker {

    public static DrawMarker INSTANCE;
    boolean isMarkerRotating = false;
    static Marker myOriginMArker;
    static Marker myDestinationMarker;

    public static DrawMarker getInstance(Context context) {
        INSTANCE = new DrawMarker(context);
        return INSTANCE;
    }

    private Context context;

    DrawMarker(Context context) {
        this.context = context;
    }

    public void drawOrigin(GoogleMap googleMap, LatLng location, int resDrawable, String title/*, float bearing*/) {
        Drawable circleDrawable = ContextCompat.getDrawable(context, resDrawable);

        int height = 120;
        int width = 120;
        BitmapDrawable bitmapdraw = (BitmapDrawable) circleDrawable;
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        try {
            myOriginMArker.remove();
        }catch (NullPointerException e){
         e.printStackTrace();
        }
        MarkerOptions markerOptions = new MarkerOptions()
                .position(location)
                .title(title)
                .icon(BitmapDescriptorFactory.fromBitmap((smallMarker)));

        myOriginMArker = googleMap.addMarker(markerOptions);
    }

    public void drawDestination(GoogleMap googleMap, LatLng location, int resDrawable, String title) {
        Drawable circleDrawable = ContextCompat.getDrawable(context, resDrawable);

        int height = 50;
        int width = 50;
        BitmapDrawable bitmapdraw = (BitmapDrawable) circleDrawable;
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        try {
            myDestinationMarker.remove();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        MarkerOptions markerOptions = new MarkerOptions()
                .position(location)
                .title(title)
                .icon(BitmapDescriptorFactory.fromBitmap((smallMarker)));

        myDestinationMarker = googleMap.addMarker(markerOptions);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
